CREATE TABLE `StockInfo` (
 `id` bigint NOT NULL AUTO_INCREMENT,
 `buyOrSell` varchar(255) DEFAULT NULL,
 `price` double NOT NULL,
 `statusCode` int NOT NULL,
 `stockTicker` varchar(255) DEFAULT NULL,
 `timeStamp` datetime DEFAULT NULL,
 `volume` bigint DEFAULT NULL,
 `cancelled` int NOT NULL,
 PRIMARY KEY (`id`)
);

INSERT INTO StockInfo (id, buyOrSell, price, statusCode, stockTicker, timeStamp, volume, cancelled) VALUES (1, 'buy', 2020.21, 2, 'AAPL', curdate(), 200, 0);
INSERT INTO StockInfo (id, buyOrSell, price, statusCode, stockTicker, timeStamp, volume, cancelled) VALUES (2, 'sell', 8721.90, 2, 'GS', curdate(), 1322, 0);
INSERT INTO StockInfo (id, buyOrSell, price, statusCode, stockTicker, timeStamp, volume, cancelled) VALUES (3, 'buy', 3910.45, 2, 'BP',  curdate(), 12, 1);