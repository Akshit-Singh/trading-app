package com.hackathon.trading.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.hackathon.trading.entity.StockInfo;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Date;

import static org.assertj.core.api.Assertions.assertThat;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureTestDatabase
@AutoConfigureMockMvc
public class StockInfoControllerTest {

    private static final String baseRestUrl ="/api/v1/stocks";

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void stocksEndpointIsOk() throws Exception {
        MvcResult mvcResult = this.mockMvc.perform(get(baseRestUrl))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn();
    }

    @Test
    public void stocksEndpointIsNotOk() throws Exception {
        MvcResult mvcResult = this.mockMvc.perform(get("/api/v1/stonks"))
                .andDo(print())
                .andExpect(status().isNotFound())
                .andReturn();
    }

    @Test
    public void createOrderIsOk() throws Exception {
        long id = 1;
        String tickerName = "C";
        double price = 2493.96;
        Long volume = 345621L;
        String buyOrSell = "buy";
        int statusCode = 0;
        Date timeStamp = new Date(System.currentTimeMillis());
        int cancelled = 0;

        StockInfo order = new StockInfo();

        order.setId(id);
        order.setPrice(price);
        order.setStockTicker(tickerName);
        order.setVolume(volume);
        order.setBuyOrSell(buyOrSell);
        order.setStatusCode(statusCode);
        order.setTimeStamp(timeStamp);
        order.setCancelled(cancelled);

        ObjectMapper mapper = new ObjectMapper();
        ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
        String requestJson = ow.writeValueAsString(order);

        MvcResult mvcResult = this.mockMvc.perform(post(baseRestUrl)
                .header("Content-Type", "application/json")
                .content(requestJson))
                .andDo(print()).andExpect(status().isCreated()).andReturn();

        order = new ObjectMapper().readValue(
                mvcResult.getResponse().getContentAsString(),
                new TypeReference<StockInfo>() { });

        assertThat(order.getId()).isGreaterThan(0);
        assertThat(order.getStockTicker()).isEqualTo(tickerName);
        assertThat(order.getPrice()).isEqualTo(price);
        assertThat(order.getVolume()).isEqualTo(volume);
    }

    // TODO: Edit order
    @Test
    public void editOrderIsOk() throws Exception {
        long id = 1;
        String tickerName = "C";
        double price = 2493.96;
        Long volume = 9504L;
        String buyOrSell = "sell";
        int statusCode = 0;
        Date timeStamp = new Date(System.currentTimeMillis());
        int cancelled = 0;

        StockInfo testOrder = new StockInfo();

        testOrder.setId(id);
        testOrder.setPrice(price);
        testOrder.setStockTicker(tickerName);
        testOrder.setVolume(volume);
        testOrder.setBuyOrSell(buyOrSell);
        testOrder.setStatusCode(statusCode);
        testOrder.setTimeStamp(timeStamp);
        testOrder.setCancelled(cancelled);

        ObjectMapper mapper = new ObjectMapper();
        ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
        String requestJson = ow.writeValueAsString(testOrder);

        System.out.println(requestJson);

        // 2. call method under test
        MvcResult mvcResult = this.mockMvc.perform(put("/api/v1/stocks/")
                .header("Content-Type", "application/json")
                .content(requestJson))
                .andDo(print()).andExpect(status().isOk()).andReturn();

        // 3. verify the results
        StockInfo order = new ObjectMapper().readValue(
                mvcResult.getResponse().getContentAsString(),
                new TypeReference<StockInfo>() { });

        System.out.println("ID:" + order.getId());

        assertThat(order.getStockTicker()).isEqualTo(tickerName);
        assertThat(order.getPrice()).isEqualTo(price);
        assertThat(order.getVolume()).isEqualTo(volume);

        // verify the edit took place
        mvcResult = this.mockMvc.perform(get(baseRestUrl + "/" + id))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn();

        order = new ObjectMapper().readValue(
                mvcResult.getResponse().getContentAsString(),
                new TypeReference<StockInfo>() { });

        assertThat(order.getId()).isEqualTo(id);
        assertThat(order.getStockTicker()).isEqualTo(tickerName);
        assertThat(order.getPrice()).isEqualTo(price);
        assertThat(order.getVolume()).isEqualTo(volume);
    }
}
